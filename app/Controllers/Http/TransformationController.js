'use strict'
const Character = use('App/Models/Character')

class TransformationController {
    async index ({ request, response, params, auth }) {
        const character = await Character.findOrFail(params.character_id)
        const transformations = await character.transformations().fetch()
        return response.ok(transformations)
    }
}

module.exports = TransformationController
