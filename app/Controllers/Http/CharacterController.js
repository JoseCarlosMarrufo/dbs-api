'use strict'
const Character = use('App/Models/Character')

class CharacterController {
    async index ({ request, response, auth }) {
        const characters = await Character.all()
        return response.ok(characters)
    }
}

module.exports = CharacterController
