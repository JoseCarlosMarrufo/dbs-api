'use strict'
const Character = use('App/Models/Character')

class DetailController {
    async index ({ request, response, params, auth }) {
        const character = await Character.findOrFail(params.character_id)
        const details = await character.details().fetch()
        return response.ok(details)
    }
}

module.exports = DetailController
