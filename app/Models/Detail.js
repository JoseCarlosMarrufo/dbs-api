'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Detail extends Model {
    static boot () {
        super.boot()
        this.addTrait('NoTimestamp')
    }

    character () {
        return this.belongsTo('App/Models/Character')
    }
}

module.exports = Detail
