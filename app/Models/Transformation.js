'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Transformation extends Model {
    character () {
        return this.belongsTo('App/Models/Character')
    }
}

module.exports = Transformation
