'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Character extends Model {
    static boot () {
        super.boot()
        this.addTrait('NoTimestamp')
    }

    transformations () {
        return this.hasMany('App/Models/Transformation')
    }

    details () {
        return this.hasOne('App/Models/Detail')
    }
}

module.exports = Character
