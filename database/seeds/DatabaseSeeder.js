'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/
const CharacterSeeder = require('./CharacterSeeder')
const UserSeeder = require('./UserSeeder')
const DetailSeeder = require('./DetailSeeder')
const TransformationSeeder = require('./TransformationSeeder')

class DatabaseSeeder {
  async run () {
    await UserSeeder._run()
    await CharacterSeeder._run()
    await TransformationSeeder._run()
    await DetailSeeder._run()
  }
}

module.exports = DatabaseSeeder
