'use strict'

/*
|--------------------------------------------------------------------------
| CharacterSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const { characters } = use('./data.json') 
class CharacterSeeder {
  async run () {
    
  }

  static async _run() {
    for (const character of characters) {
      await Factory.model('App/Models/Character').create(character)
    }
  }
}

module.exports = CharacterSeeder
