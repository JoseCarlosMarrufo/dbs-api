'use strict'

/*
|--------------------------------------------------------------------------
| TransformationSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const { transformations } = use('./data.json')

class TransformationSeeder {
  async run () {
    
  }

  static async _run() {
    for (const transformation of transformations) {
      await Factory.model('App/Models/Transformation').create(transformation)
    }
  }
}

module.exports = TransformationSeeder
