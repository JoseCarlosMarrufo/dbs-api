'use strict'

/*
|--------------------------------------------------------------------------
| DetailSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const { details } = use('./data.json')

class DetailSeeder {
  async run () {
    
  }

  static async _run() {
    for (const detail of details) {
      await Factory.model('App/Models/Detail').create(detail)
    }
  }
}

module.exports = DetailSeeder
