'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

Factory.blueprint('App/Models/User', (faker, i, data) => {
  return {
    email: data.email,
    password: data.password,
    is_active: data.is_active
  }
})

Factory.blueprint('App/Models/Character', (faker, i, data) => {
    return {
      name: data.name,
      nickname: data.nickname,
      technique: data.technique,
      is_active: data.is_active,
      image: data.image
    }
})

Factory.blueprint('App/Models/Transformation', (faker, i, data) => {
    return { 
      character_id: data.character_id,
      name: data.name,
      image: data.image
    }
})

Factory.blueprint('App/Models/Detail', (faker, i, data) => {
    return { 
      character_id: data.character_id,
      description: data.description,
      power: data.power
    }
})
