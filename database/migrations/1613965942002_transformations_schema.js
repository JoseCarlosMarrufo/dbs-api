'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TransformationsSchema extends Schema {
  up () {
    this.create('transformations', (table) => {
      table.bigIncrements()
      table.bigInteger('character_id').unsigned().references('id').inTable('characters')
      table.string('name', 100).notNullable()
      table.string('image').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('transformations')
  }
}

module.exports = TransformationsSchema
