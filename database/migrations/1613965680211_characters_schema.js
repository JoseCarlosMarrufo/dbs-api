'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CharactersSchema extends Schema {
  up () {
    this.create('characters', (table) => {
      table.bigIncrements()
      table.string("name", 100).notNullable()
      table.string('nickname', 100).nullable()
      table.string('technique', 100).nullable()
      table.boolean('is_active').defaultTo(true)
      table.string("image").nullable()
    })
  }

  down () {
    this.drop('characters')
  }
}

module.exports = CharactersSchema
