'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetailsSchema extends Schema {
  up () {
    this.create('details', (table) => {
      table.bigIncrements()
      table.bigInteger('character_id').unsigned().references('id').inTable('characters')
      table.text('description')
      table.integer('power')
    })
  }

  down () {
    this.drop('details')
  }
}

module.exports = DetailsSchema
